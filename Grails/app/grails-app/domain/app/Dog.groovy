package app

import grails.rest.*

@Resource(uri='/dog', formats=['json'])
class Dog extends Animal {

    Boolean has_tail

    static mapping = {
        has_tail column: true
    }


    static constraints = {
    }
}
