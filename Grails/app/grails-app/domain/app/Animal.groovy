package app

class Animal {

    String name
    Integer legs

    static mapping = {
        legs column: 4
    }

    static constraints = {
        name blank:false
    }
}
