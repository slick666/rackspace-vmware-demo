#!/bin/bash

printenv DJANGO_SETTINGS_MODULE

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Loop sleep until the database container comes online
echo "Waiting for db to accept connections ..."
while ! nc -w 1 postgres 5432 2>/dev/null
  do
    echo "still waiting"
    sleep 1
  done


# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Start server
echo "Starting server"
/usr/local/bin/gunicorn app.wsgi:application -w 2 -b :5000