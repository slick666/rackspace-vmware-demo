# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Animal(models.Model):
    name = models.CharField(max_length=30, blank=False)
    legs = models.IntegerField(default=4)


class Dog(Animal):
    has_tail = models.BooleanField(default=True)

