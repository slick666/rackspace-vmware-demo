# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from rest_framework import viewsets

from app.rescue.models import Dog
from app.rescue.serializers import DogSerializer


class DogViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Dog.objects.all().order_by('name')
    serializer_class = DogSerializer
