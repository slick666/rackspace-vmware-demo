# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-06 04:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Animal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('legs', models.IntegerField(default=4)),
            ],
        ),
        migrations.CreateModel(
            name='Dog',
            fields=[
                ('animal_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='rescue.Animal')),
                ('has_tail', models.BooleanField(default=True)),
            ],
            bases=('rescue.animal',),
        ),
    ]
