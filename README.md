# Rackspace Demo
This project is the answer the take home quiz for Landon Jurgens. Submitted to Rackspace on July 5th, 2017.

### Stated Question
The task: Create a small app with Grails (using Groovy)

* Use a 3.x version of Grails
    * You will need JDK 8 installed, if you don’t want to install Java on your main system.
    * The easiest way to install Grails (and Groovy, although you don’t strictly need to install Groovy) is via sdkman. The Grails site should walk you through this.
* Please store your code in github.com
* The application should be database backed.
    * The default in-memory database that you get by default with Grails is perfectly fine.
* I’m looking for a simple CRUD app.
    * CRUD = Create, Read (list all, display one), Update, Delete.
    * This can be browser / web  based OR this can be REST based. Your choice.
        * If REST based, POSTMAN would be a good choice to exercise your app
* A single Domain class is fine
    * The domain class can be whatever you want. A Book, a DVD, a CD, a pet.
    * The domain class only needs a few fields, no need to go crazy
    * The domain class should have constraints to restrict to valid values such as blank, nullable, minimum, maximum, etc.
* It is not necessary to create unit tests, but if you find some time to look at Spock (the Unit and Integration test framework we use) I’d be interest in your comparisons with what you’ve used in Python.

## Setup and Use
TBD


# Explaination

## Grails Implementation

## Python implementation